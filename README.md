# STM32F407ZGT6+L298N：两路PWM输出实现电机转速调整

## 项目描述

本项目基于STM32F407ZGT6微控制器和L298N电机驱动模块，实现了两路PWM输出，用于控制两个电机的转速。通过配置GPIO引脚，可以实现电机的正反转控制和使能控制。

## 硬件要求

- **STM32F407ZGT6微控制器**
- **L298N电机驱动模块**
- **两个电机**
- **12V电源**

## 硬件连接

- **IN0 (D0)** 和 **IN1 (D1)**：连接到STM32F407ZGT6的GPIO引脚，用于控制电机A的正反转。
- **IN2 (B5)** 和 **IN3 (B6)**：连接到STM32F407ZGT6的GPIO引脚，用于控制电机B的正反转。
- **PF8**：连接到L298N的使能端，用于控制电机A的使能。
- **PF7**：连接到L298N的使能端，用于控制电机B的使能。

## 功能实现

1. **PWM输出**：通过配置STM32F407ZGT6的定时器，生成两路PWM信号，分别控制两个电机的转速。
2. **正反转控制**：通过控制IN0、IN1、IN2、IN3的电平状态，实现电机的正转、反转和停止。
3. **使能控制**：通过控制PF8和PF7的电平状态，实现电机的使能和禁能。

## 参考资料

详细的文章和实现过程可以参考以下链接：
[CSDN博客文章](https://blog.csdn.net/m0_59671068/article/details/124164241)

## 使用说明

1. 按照硬件连接图连接STM32F407ZGT6和L298N模块。
2. 下载并导入项目代码到STM32开发环境中。
3. 编译并烧录代码到STM32F407ZGT6微控制器。
4. 通过修改代码中的PWM占空比和GPIO状态，实现对电机转速和方向的控制。

## 贡献

欢迎大家提出问题和改进建议，可以通过提交Issue或Pull Request来参与项目的开发。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。